import React, { Component } from 'react';
import {Badge,
        Form,
        FormGroup,
        Label,
        Input,
        Button,
        Table,Container
       } from 'reactstrap';

class App extends Component {
  constructor(props)
  {

    super(props);
    this.state=
    {
      tabla:{},
      opcion:1      
    }
    this.foto="";
    this.enviarSolicitud =this.enviarSolicitud.bind(this);
    this.verHistorial=this.verHistorial.bind(this);
    this.regresar=this.regresar.bind(this);
    this.mostrar=this.mostrar.bind(this);
    this.bucketName ="";
    this.keyName = "";


  }
  render() {
    return (
      <div>
         <div align ="center" >
              <h1>
                  <Badge color="light">
                      Practica uno de seminario
                    </Badge>    
                </h1> 
                {this.opcionPantalla()}
         </div>
         
      </div>
    );
  }
  
  opcionPantalla()
  {
     if(this.state.opcion<2)
     {
       return this.agregarCatalogo();
     }
     else
     {
       return this.historial();
     }

  }


  agregarCatalogo()
  {
    return(
        <div align ="justify"> 
              <h1>
                  <Badge color="light">
                            Agregar Juego a Catalogo
                          </Badge>
              </h1>
              <Form onSubmit={this.enviarSolicitud}>
                  <FormGroup >
                      <Label for="nombre">Nombre Juego</Label>
                      <Input type="text" id="nombre"/>
                  </FormGroup>
                  <FormGroup>
                      <Label for="fecha">Fecha</Label>
                      <Input
                        type="date"
                        name="datetime"
                        id="fecha"
                        placeholder="datetime placeholder"
                      />
                  </FormGroup>
                  <FormGroup>
                        <Label for="compañia">Compañia</Label>
                        <Input
                            type="text"
                            id="compañia"
                        />
                  </FormGroup>
                  <FormGroup>
                        <Label for="palabras">Palabras Clave</Label>
                        <Input
                            type="textarea"
                            id="palabras"
                        />
                  </FormGroup>
                  <FormGroup>
                      <input type="file" id="file" accept="image/*" onChange={this.mostrar}/>
                      <br></br>
                      <img id="img" src="" alt=""></img>
                    </FormGroup>
                  <FormGroup>
                      <Button>Agregar Catalogo</Button>
                      <Button onClick={this.verHistorial}> Ver Historial </Button>
                  </FormGroup>

                </Form>
         </div>
    );
  }
  mostrar(e){
    e.preventDefault();
    var archivo = document.getElementById("file").files[0];
    var reader = new FileReader();
    if (e.file) {
      reader.readAsDataURL(archivo );
      reader.onloadend = function () {
        document.getElementById("img").src = reader.result;
        console.log("entro");
        this.foto="adsfdsafasd";
      }
    }
    
  }
  enviarSolicitud(e)
  {
    e.preventDefault();
    var nombre =e.target.nombre.value;
    var fecha = e.target.fecha.value;
    var compañia = e.target.compañia.value;
    var palabras =e.target.palabras.value;
    this.mandar(nombre,fecha,compañia,palabras);  
    
  }
  mandar(n,f,c,p,fo)
  {
    //http://34.220.172.154:3000/
    fetch('http://34.220.172.154/mandar?n='+n+'&f='+f+'&c='+c+'&p='+p)
    .then(function(response) {
       return response.json();
    })
    .then(function(data) {
        console.log('data = ', data);
    })
    .catch(function(err) {
        console.error(err);
    }); 

     
  }

  
  verHistorial(e)
  {
    e.preventDefault();
    this.hacerConsultaBD();
    
  };

  historial()
  {
    return(
        <div align ="justify">
        <Container>
              <h1>
                  <Badge color="light">
                            Agregar Juego a Catalogo
                          </Badge>
              </h1>
               {this.hacerConsultaBD()}               
               <Table dark>
                <thead>
                  <tr>
                    <th>Nombre del bucket</th>
                 </tr>
                 </thead> 
                 <tbody>
                   {this.mostrart()}
                   </tbody>
                 </Table>
              <Button onClick={this.regresar}> Regresar a Agregar Catalogo </Button>
        </Container>
            
          </div>

    );
  }
  mostrart()
  {
     console.log(this.state.tabla);
     return(
        <td>
            {
              this.state.tabla.map((i)=>{
                return(<tr>
                      {i.nombre}
                </tr>);
              })
            }
          </td>);
  }
  hacerConsultaBD()
  {
    //http://34.220.172.154:3000/
    fetch('http://34.220.172.154:3000/consulta')
    .then((response) => {
      return response.json()
    })
    .then((empleados) => {
      this.setState({
        tabla:empleados,
        opcion:3
      })    
    })
    .catch(function(e){
      console.log(e);
    })
  }
  regresar(e)
  {
    e.preventDefault();
    this.setState({
      opcion:1
    })
  }
  
  
}

export default App;
